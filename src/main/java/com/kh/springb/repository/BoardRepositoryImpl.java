package com.kh.springb.repository;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.kh.springb.entity.Board;

@Repository
public class BoardRepositoryImpl implements boardRepository{
	
	@Autowired
	private SqlSession sqlSession;
	
	@Override
	public void insertBoard(Board board) {
		
		int count = sqlSession.insert("board.insert",board);
		System.out.println(count);
	}

	@Override
	public List<Board> selectList() {
		List<Board> list = sqlSession.selectList("board.list"); 
		
		return list;
	}

	@Override
	public Board selectNo(int no) {
		Board result = sqlSession.selectOne("board.bno", no);
		return result;
	}

	@Override
	public void update(Board board) {
		sqlSession.update("board.update", board);

		
	}

	@Override
	public void delete(int bno) {
		System.out.println("dao 의 bno  :" + bno);
		
		sqlSession.delete("board.delete", bno);
		
	}

}
