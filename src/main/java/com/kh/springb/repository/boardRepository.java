package com.kh.springb.repository;

import java.util.List;

import com.kh.springb.entity.Board;

public interface boardRepository {

	void insertBoard(Board board);
	
	List<Board> selectList();
	
	Board selectNo(int no);
	
	void update(Board board);

	void delete(int bno);
}
