package com.kh.springb.service;

import java.util.List;

import com.kh.springb.entity.Board;

public interface BoardService {

	//목표 : 인설트
	void boardInsert(Board board);
	
	//select
	List<Board> selectList();
	
	Board selectNo(int no);
	
	//update
	int update(Board board);

	void delete(int bno);
}
