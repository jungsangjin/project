package com.kh.springb.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.kh.springb.entity.Board;
import com.kh.springb.repository.boardRepository;

import oracle.jdbc.replay.driver.ReplayStatisticsImpl;

@Service
public class BoardServiceImpl implements BoardService{

	
	@Autowired
	private boardRepository repository;
	
	public void boardInsert(Board board) {
		
		repository.insertBoard(board);
	}

	@Override
	public List<Board> selectList() {
		List<Board> list =repository.selectList();
		
		return list;
		
	}

	@Override
	public Board selectNo(int no) {
		Board result = repository.selectNo(no);
		return result;
	}

	@Override
	public int update(Board board) {
		repository.update(board);
		
		Board result = this.selectNo(board.getBno());
		
		
		return result.getBno();
		
	}

	@Override
	public void delete(int bno) {
		 repository.delete(bno);
		 System.out.println("서비스 bno :" + bno);
		 
		
	};
}
