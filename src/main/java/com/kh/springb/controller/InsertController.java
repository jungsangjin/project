package com.kh.springb.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.kh.springb.entity.Board;
import com.kh.springb.service.BoardService;

@Controller
@RequestMapping("board")
public class InsertController {


	@Autowired
	private BoardService service;


	//url 일때동작하는 애 
	//url 값 쿼리스트링//  /insert?no=1&type=number
	// @RequestParam int no,  ~ String type
	// @requestParam(value = "no") int number
	@GetMapping("/insert")
	public String insert() {

		return "board/boardInsert";
	}

	//임의적으로 post를

	//form 에서 post 넘길때만 동작
	@PostMapping("/insert")
	public String insert(@ModelAttribute Board board) {


		service.boardInsert(board);

		//redirect 302코드 랑 리다이렉트할 주소를 보내여 // 주소jsp 가 아니라 주소입니다
		return "redirect:list";
	}

	@GetMapping("/list")
	public String list(Model model) {

		//jsp에 값을 전달할거예여 
		List<Board> list = service.selectList();
		model.addAttribute("list", list);

		return "board/list";
	}

	@GetMapping("/detail")
	public String detail(@RequestParam int bno, Model model) {

		Board result = service.selectNo(bno);

		model.addAttribute("result", result);

		return "board/detail";
	}

	@PostMapping("update")
	public String update(@RequestParam int bno, Model model) {

		//조회
		Board result = service.selectNo(bno);

		model.addAttribute("result", result);


		return "board/update";
	}
	
	@PostMapping("/update2")
	public String update(@ModelAttribute Board board) {
		
		int no = service.update(board);
		//1. 수정페이지에서  정보를 가지고
		// no를 기준으로 update한 후에 , detail 로 redirect 할거예여
		
		return "redirect:detail?bno="+no;
	}
	
	@PostMapping("/delete")
	public String delete(@ModelAttribute Board board) {
		System.out.println("컨트롤러 bno : " + board.getBno());
		service.delete(board.getBno());
		
		return "redirect:list";
		
	}


}
