<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>insert</title>
    <style>
        form{
            width: 400px;
            margin: 50px auto;
            /*background: pink;*/
            padding:30px; 
        }
        .title{
            width: 400px;
            height: 35px;
            display: block;
             margin: 10px 0 20px 0 ;
             font-size: 1em;
			color: #979797;
			font-weight: 300;
    
        }
        .content{
            width: 400px;
            height: 300px;
            display: block;
            margin: 10px 0 20px 0 ;
            padding: 10px 5px;
			font-family: "나눔고딕";
			color: #5F5F5F;
        }
        .submit{
          width: 80px; 
        height: 30px
            
        }
        .submit:hover{
            background: pink;
            border: none;
            border-radius: 4px;
        }
    </style>
</head>
<body>
   <form action ="update2" method="post" id="change">
   		<input type="hidden" value="${result.bno }" name="bno">
       글제목 : <input type="text" name="title" class="title" value="${result.title }">
       내용 : <textarea class="content" name ="content" maxlength="1000">${result.content }</textarea>

		<input type="submit" value="수정완료" name="update">     
		       
   </form>
    
</body>
</html>