<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <title> search </title>
    <style>
        a{ text-decoration: none; color: #333;}
        tbody{
           
            text-align: center;
        }
        .pager{
            overflow: hidden;
            list-style: none;
        }
        .pager li{
            float: left;
            padding: 10px 10px;
        }
    </style>
    
</head>
<body>
    
   <article>
        <div class="conWrap">
        <form action="" method="">
        
       

            <table class="oneTable" >
                <colgroup>
                    <col width="10%">
                    <col width="50%">
                    <col width="10%">
                    <col width="10%">
                    <col width="20%">
                </colgroup>
                <thead>
                    <tr>
                        <th>인덱스</th>
                        <th>제목</th>
                        <th>조회수</th>
                        <th>날짜</th>
                    </tr>
                </thead>
                <tbody>
                	<c:forEach var="list" items="${list}">
                		
                		 <tr>
                        <td>${list.bno}</td>
                        <td>${list.title }</td>
                        <td>${list.count }</td>
                        <td>${list.bdate }</td>
                    </tr>
                	</c:forEach>
                </tbody>

            </table>

            <ul class="pager mt40">
                <li> &lt; &gt;</li>
                <li>1</li>
                <li>2</li>
                <li>3</li>
                <li>4</li>
                <li>5</li>
                <li>>></li>
            </ul>
             </form>
            </div>
        </article>
        
        
        <script type="text/javascript">
        	
        </script>
</body>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>


<script type="text/javascript">

	$(function(){
		$("tbody").find("tr").mouseenter(function() {
			$(this).css({"backgroundColor" : "pink"});
		}).mouseleave(function(){
			$(this).css({"backgroundColor" : "white"});
			
		});	
		
		$("tbody").find("tr").click(function(){
			var idx = $(this).find("td").eq(0).text();
			console.log(idx);
			
			location.href = "http://127.0.0.1:8001/springb/board/detail?bno="+idx;
		});
		
		
	});
</script>
</html>