<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>insert</title>
    <style>
        form{
            width: 400px;
            margin: 50px auto;
            /*background: pink;*/
            padding:30px; 
        }
        .title{
            width: 400px;
            height: 35px;
            display: block;
             margin: 10px 0 20px 0 ;
             font-size: 1em;
			color: #979797;
			font-weight: 300;
    
        }
        .content{
            width: 400px;
            height: 300px;
            display: block;
            margin: 10px 0 20px 0 ;
            padding: 10px 5px;
			font-family: "나눔고딕";
			color: #5F5F5F;
        }
        .submit{
          width: 80px; 
        height: 30px
            
        }
        .submit:hover{
            background: pink;
            border: none;
            border-radius: 4px;
        }
    </style>
</head>
<body>
   <form action ="insert" method="post">
       글제목 : <input type="text" placeholder="제목" name="title" class="title">
       내용 : <textarea class="content" name ="content" maxlength="1000"> </textarea>
     
       <input type="submit" value ="글쓰기" class="submit"/> 
       
   </form>
    
</body>
</html>