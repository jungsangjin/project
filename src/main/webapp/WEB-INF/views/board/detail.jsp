<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>insert</title>
    <style>
        form{
            width: 400px;
            margin: 50px auto;
            /*background: pink;*/
            padding:30px; 
        }
        .title{
            width: 400px;
            height: 35px;
            display: block;
             margin: 10px 0 20px 0 ;
             font-size: 1em;
			color: #979797;
			font-weight: 300;
    
        }
        .content{
            width: 400px;
            height: 300px;
            display: block;
            margin: 10px 0 20px 0 ;
            padding: 10px 5px;
			font-family: "나눔고딕";
			color: #5F5F5F;
        }
        .submit{
          width: 80px; 
        height: 30px
            
        }
        .submit:hover{
            background: pink;
            border: none;
            border-radius: 4px;
        }
    </style>
</head>
<body>
   <form action ="" method="post" id="change">
   	<input type="hidden" value="${result.bno }" name="bno">
 
       글제목 : <input type="text" name="title" class="title" value="${result.title }" readonly>
       내용 : <textarea class="content" name ="content" maxlength="1000" readonly>${result.content }</textarea>
   </form>
		<button type="submit" value="수정" name="update" id="update">수정</button>
		<button type="submit" value="삭제" name="delete" id="delete">삭제</button> 
	<button id="list">목록</button>	
	
</body>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript">

	$("#update").click(function(){
		//일단은 저희가 수정하려면 detail 다시불러내죠 no = 5 // no = 5 조회를 헤야하죠
		$("#change").attr("action", "update");
		$("#change").submit();
	});
	
	$("#delete").click(function(){
		
		var check = confirm("삭제하시겠습니까?")
		if(check){
		$("#change").attr("action", "delete");
		$("#change").submit();
		}
		else {
				
		}
	});
	
	$("#list").click(function(){
		location.href="/springb/board/list";
		
	})
</script>
</html>